#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 17:25:11 2019

@author: vbokharaie
"""
from mitfat import Lib
__methods__ = [] # self is a DataStore
register_method = Lib.register_method(__methods__)

#%%
@register_method
def _save_clusters(self, X_train, data_label, cluster_labels, cluster_centroids):
    import pandas as pd
    import numpy as np
    import os
    no_clusters = np.unique(cluster_labels).shape[0]
    dir_save_subfolder = os.path.join(self.dir_save, \
                                      '02_clusters', \
                                      data_label+'_clusters_'+str(no_clusters))
    if not os.path.exists(dir_save_subfolder):
        os.makedirs(dir_save_subfolder)
    centroid_length = np.shape(cluster_centroids)[1]

    column_names = []
    my_index = np.arange(centroid_length)
    df_centroids = pd.DataFrame(columns=column_names, index=my_index)

    if centroid_length == self.num_time_steps:
        df_centroids['Time'] = self.time_steps
    elif centroid_length == 1:
        df_centroids['Index'] = 1
    else:
        my_indices = np.arange(centroid_length) + 1
        df_centroids['Segments'] = my_indices

    for cc_ in np.arange(no_clusters):
        df_centroids['Cluster_'+str(cc_+1)] = cluster_centroids[cc_, :]

    filename_csv = os.path.join(dir_save_subfolder, 'Cluster_centres.xlsx')
    df_centroids.to_excel(filename_csv, index=False)
