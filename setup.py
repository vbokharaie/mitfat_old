import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="MiTfAT",
    version="0.1.1",
    author="Vahid S. Bokharaie",
    author_email="vahid.bokharaie@tuebingen.mpg.de",
    description="A pyhton-based fMRI Analysis Tool.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.tuebingen.mpg.de/vbokharaie/mitfat",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)