#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 18:21:14 2019

@author: vbokharaie
"""
if_verbose = False

if_save_eps = True

if_debug = False

if_plot_basics = True
if_plot_lin_reg = True
if_plot_raw = True

if_cluster = True
if_cluster_hiararchical = True

if_detrend = True

if_pca = False
