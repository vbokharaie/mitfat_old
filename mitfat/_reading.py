#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 11:55:53 2019

@author: vbokharaie
"""


#%%
def convert_real_time_to_index(times_files, cutoff_times_to_convert):
    import numpy as np
    text_file = open(times_files, "r")
    time_steps = text_file.read().strip().split('\n')
    n_last = len(time_steps)-1
    n_0 = 0
    times_all = np.zeros(len(time_steps))
    for cc23, one_line in enumerate(time_steps):
        times_all[cc23] = np.float(one_line)
    output_list = []
    for idx, my_time in enumerate(cutoff_times_to_convert):
        bb_d = [i for i, v in enumerate(times_all) if v > my_time]
        output_list.append(bb_d[0])
    output_list.insert(0, n_0)
    output_list.append(n_last)
    return output_list



#%% read info file
def read_info_file(info_file_name):
    import os
    import pathlib
    # dir_path = os.path.dirname(os.path.realpath(__file__))
    # info_file_name = os.path.join(dir_path, info_file_name)
    dir_file_parent = pathlib.Path(__file__).parent
    print('++++++++++++++++++++', dir_file_parent)
    dir_infofile = os.path.dirname(info_file_name)
    print('xxxxxxxx',dir_infofile)
    if dir_infofile == '':
        dir_infofile = os.path.dirname(__file__)
        print('*************',dir_infofile)
        info_file_name = os.path.join(dir_infofile, info_file_name)
    with open(info_file_name) as f:
        info_file_content = f.readlines()
    info_file_content = [x.strip() for x in info_file_content]  # remove '\n'
    info_file_content = [x.lstrip() for x in info_file_content] # remove leading white space
    flag_data_file = False
    flag_mask_file = False
    flag_time_file = False
    flag_dir_source = False
    flag_dir_save = False
    flag_mol_name = False
    flag_exp_name = False
    flag_dataset_no = False
    flag_description = False
    flag_cutoff_times = False
    flag_signal_name = False

    for line in info_file_content:
        if line[:9].lower() == 'DATA_FILE'.lower():
            data_file_name = line[10:].strip()
            flag_data_file = True
        elif line[:9].lower() == 'MASK_FILE'.lower():
            mask_file_name = line[10:].strip()
            flag_mask_file = True
        elif line[:9].lower() == 'TIME_FILE'.lower():
            time_file_name = line[10:].strip()
            flag_time_file = True
        elif line[:10].lower() == 'DIR_SOURCE'.lower():
            dir_source = line[11:].strip()
            dir_source = pathlib.Path(dir_source)
            assert dir_source.is_dir(),\
                "'DIR_SOURCE' not a valid folder path."
            if not dir_source.is_absolute():
                dir_source = dir_source.resolve()

#            if dir_source[:2] == './':
#                dir_source = dir_source[2:]
#                dir_source = os.path.join(dir_infofile, dir_source)
#            elif dir_source[:3] == '../':
#                dir_source = dir_source[3:]
#                dir_source = os.path.join(dir_file_parent, dir_source)
            flag_dir_source = True
        elif line[:8].lower() == 'DIR_SAVE'.lower():
            dir_save = line[9:].strip()
            dir_save = pathlib.Path(dir_save)
            assert dir_save.is_dir(),\
                "'DIR_SOURCE' not a valid folder path."
            if not dir_save.is_absolute():
                dir_save = dir_save.resolve()

#            if dir_save[:2] == './':
#                dir_save = dir_save[2:]
#                dir_save = os.path.join(dir_infofile, dir_save)
#            elif dir_save[:3] == '../':
#                dir_save = dir_save[3:]
#                dir_save = os.path.join(dir_file_parent, dir_save)

            flag_dir_save = True
        elif line[:8].lower() == 'MOL_NAME'.lower():
            mol_name = line[9:].strip()
            flag_mol_name = True
        elif line[:8].lower() == 'EXP_NAME'.lower():
            exp_name = line[9:].strip()
            flag_exp_name = True
        elif line[:5].lower() == 'DS_NO'.lower():
            dataset_no = line[6:].strip()
            try:
                dataset_no = int(dataset_no)
            except ValueError:
                raise TypeError("Only integers are allowed for DS_NO in " + info_file_name)
            flag_dataset_no = True
        elif line[:4].lower() == 'DESC'.lower():
            description = line[5:].strip()
            flag_description = True
        elif line[:11].lower() == 'SIGNAL_NAME'.lower():
            signal_name = line[12:].strip()
            flag_signal_name = True
        elif line[:11].lower() == 'EVENT_TIMES'.lower():
            cutoff_times_raw = line[12:].strip()
            if cutoff_times_raw == '':
                pass
            else:
                cutoff_times_raw = cutoff_times_raw.split(",")
                cutoff_times = []
                for el in cutoff_times_raw:
                    current_time = float(el.strip())
                    cutoff_times.append(current_time)
            flag_cutoff_times = True
    if not flag_dir_source:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        dir_path = pathlib.Path(dir_path)
        dir_path = dir_path.resolve()
        dir_parent = dir_path.parent
        dir_source = os.path.join(dir_parent, 'resources', 'datasets')

    if not flag_dir_save:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        dir_path = pathlib.Path(dir_path)
        dir_path = dir_path.resolve()
        dir_parent = dir_path.parent
        dir_save_main = os.path.join(dir_parent, 'resources', 'output')

    if not flag_data_file:
        raise('Data file name is missing in info file')
    else:
        data_file_name = os.path.join(dir_source, data_file_name)
    if not flag_mask_file:
        raise('Mask file name is missing in info file')
    else:
        mask_file_name = os.path.join(dir_source, mask_file_name)
    if not flag_time_file:
        time_file_name = ''
    else:
        time_file_name = os.path.join(dir_source, time_file_name)


    if not flag_dataset_no:
        dataset_no = ''
        dir_save_ds = ''
    else:
        dir_save_ds = 'DS_' + str(dataset_no)

    if not flag_exp_name:
        exp_name = 'noname'
    dir_save_ds = dir_save_ds + '_exp_'+exp_name

    if not flag_mol_name:
        mol_name = ''
    else:
        dir_save_ds = dir_save_ds + '_mol_' + mol_name

    if not flag_signal_name:
        signal_name = 'unknown_signal'
    dir_save = os.path.join(dir_save_main, dir_save_ds, signal_name)
    if not flag_description:
        description = ''
    if not flag_cutoff_times:
        cutoff_times = ''
    print('++++++++++++++++++++', dir_source)

    return data_file_name, mask_file_name, time_file_name, dir_source, dir_save, \
        mol_name, exp_name, dataset_no, cutoff_times, description, signal_name

#%%
def main_get_data(info_file_name):
    import numpy as np
    import nibabel

    from mitfat import flags

    mask_roi = []
    mask_roi_numpy = []

    data_file_name, mask_file_name, time_file_name, dir_source, dir_save, \
        mol_name, experiment_name, dataset_no, cutoff_times, description, signal_name = \
            read_info_file(info_file_name)

    print('----------------------------------------')
    print('reading data from: ', dir_source)
    print('outputs will be saved in: ', dir_save)
    print('----------------------------------------')


    data_roi = nibabel.load(data_file_name)
    mask_roi = nibabel.load(mask_file_name)
    mask_roi_numpy = mask_roi.get_data()
    # following line added in case mask values are not set at 1.
    mask_roi_numpy[mask_roi_numpy != 0] = 1;
    ## applying the mask and storing data in 2D format in a list
    from nilearn.masking import apply_mask
    data_nii_masked = apply_mask(data_roi, mask_roi)
    if flags.if_debug:
        from mitfat.func_tests import check_how_nilearn_apply_mask_works
        check_how_nilearn_apply_mask_works(data_nii_masked, data_roi)

    data_nii_masked = np.abs(data_nii_masked) # in case data is recorded as negative values
    print('Masked data shape: ')
    print('Number of time_steps: ', data_nii_masked.shape[0])
    print('Number of voxels ', data_nii_masked.shape[1])
    print('----------------------------------------')

    if not time_file_name == '':
        text_file = open(time_file_name, "r")
        time_steps_raw = text_file.read().strip().split('\n')
        time_steps = np.zeros(len(time_steps_raw))
        for cc23, one_line in enumerate(time_steps_raw):
            time_steps[cc23] = np.float(one_line)
        text_file.close()
    else:
        time_steps = np.arange(data_nii_masked.shape[0])+1

    if not (time_file_name == '' and cutoff_times == ''):
        time_steps_min = np.min(time_steps)
        time_steps_max = np.max(time_steps)
        for el in cutoff_times:
            if el < time_steps_min or el > time_steps_max:
                raise ValueError('EVENT_TIMES exceed time step values in TIME_FILE in '+\
                                 info_file_name)
        indices_cuttoff = \
            convert_real_time_to_index(time_file_name, cutoff_times)
    else:
        indices_cuttoff = [0, data_nii_masked.shape[0]]


 

    return data_nii_masked, mask_roi_numpy, time_steps, signal_name,\
                 indices_cuttoff, \
                 experiment_name, dataset_no, mol_name, \
                 dir_source, dir_save, \
                 description
