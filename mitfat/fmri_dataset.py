#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 10:55:19 2018

@author: vbokharaie
"""

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
# Eight is reasonable in this case.

# %% defintion of fMRI dataset class
from mitfat import Lib
from mitfat import _basics
from mitfat import _plotting
from mitfat import _clustering
from mitfat import _saving
from mitfat import _detrending
@Lib.add_methods_from(_basics)
@Lib.add_methods_from(_plotting)
@Lib.add_methods_from(_clustering)
@Lib.add_methods_from(_saving)
@Lib.add_methods_from(_detrending)
class fmri_dataset:
    #  a variable to keep track of all created objects
    total_created_objects = 0

    ###
    def __init__(self, data_masked, mask_numpy, time_steps, signal_name,
                 indices_cutoff,
                 experiment_name, dataset_no, mol_name,
                 dir_source, dir_save,
                 description):
        import numpy as np
        from mitfat._shapes import bbox_3d, bbox_mean, bbox_3d_seq
        # main variables
        self.data_raw = data_masked
        self.data = self.data_raw  # default: raw. Can also be normalised.
        self.data_mean = np.nanmean(self.data, axis=0)
        self.mask = mask_numpy
        self.time_steps = time_steps
        self.num_voxels = self.data.shape[1]
        self.num_time_steps = self.data.shape[0]
        assert self.num_voxels == np.sum(self.mask),\
            'mask size not equal to number of voxels in data file'
        assert self.num_time_steps == len(self.time_steps),\
            'number of time-steps in data file does not match that of time file'
        # now other variables
        self.signal_name = signal_name
        self.indices_cutoff = indices_cutoff
        self.cutoff_times = [0.0]*len(indices_cutoff)
        if len(indices_cutoff) > 1:
            for idx, el in enumerate(indices_cutoff):
                self.cutoff_times[idx] = self.time_steps[el]
        else:
            self.cutoff_times[idx] = self.time_steps
        self.calc_mean_segments()
        self.experiment_name = experiment_name
        self.dataset_no = dataset_no
        self.mol_name = mol_name
        self.dir_source = dir_source
        self.dir_save = dir_save
        self.description = description
        self.data_normalised = None
        # linear regression
        self.line_reg = None
        self.line_reg_slopes = None
        self.line_reg_biases = None
#        self.calc_lin_reg()
        # bbox
        self.bbox_mask = bbox_3d(self.mask)
        self.bbox_data_mean = bbox_mean(self.bbox_mask, self.data)
        self.bbox_mask_seq = bbox_3d_seq(self.bbox_mask)
        # possible hiearachial clustering
        self.data_hierarchical = None
        self.bbox_mask_hierarchical = None
        #
        fmri_dataset.total_created_objects = fmri_dataset.total_created_objects + 1


# %%
def read_data(info_file_name):
    from mitfat._reading import main_get_data
    # read the data from file
    data_masked, mask_numpy, time_steps, signal_name,\
        indices_cutoff, experiment_name, dataset_no, mol_name,\
        dir_source, dir_save, description = main_get_data(info_file_name)

    # establish the fmri_dataset object
    fmri_object = fmri_dataset(data_masked, mask_numpy, time_steps, signal_name,
                           indices_cutoff,
                           experiment_name, dataset_no, mol_name,
                           dir_source, dir_save,
                           description)

    return fmri_object


# %% __main__
if __name__ == "__main__":
    from mitfat import flags
    from mitfat.fmri_dataset import read_data
    info_file_name = 'sample_info_file.txt'
    dataset1 = read_data(info_file_name)
    # optional fixes on the data
    if_fix_anomolies = True
    if if_fix_anomolies:
        dataset1.fix_anomolies()
    if_impute = True
    if if_impute:
        dataset1.impute()
    if_normalised = True
    if if_normalised:
        dataset1.normalise()
    dataset1.calc_lin_reg()
    # Basic plots
    if flags.if_plot_basics:
        dataset1.plot_basics()

    if flags.if_plot_lin_reg:
        dataset1.plot_basics('lin_reg')

    if flags.if_plot_raw:
        dataset1.plot_basics('raw')

    if flags.if_cluster:
        ###
        X_train = dataset1.data_normalised
        X_train_label = 'RAW_Normalised'
        print('-----------------------------------')
        print('Clustering ', X_train_label)
        for num_clusters in [2, 3, 4, 5, 6, 7, 8, 9, ]:
            print(num_clusters, 'clusters')
            cluster_labels, cluster_centroid = \
                dataset1.cluster_raw(X_train, num_clusters)
            dataset1._save_clusters(X_train, X_train_label,
                                    cluster_labels, cluster_centroid)
            dataset1._plot_clusters(X_train, X_train_label,
                                    cluster_labels, cluster_centroid)
        ###
        X_train = dataset1.data_mean
        X_train_label = 'Mean_Normalised'
        print('-----------------------------------')
        print('Clustering ', X_train_label)
        for num_clusters in [2, 3, 4, 5, 6, 7, 8, 9, ]:
            print(num_clusters, 'clusters')
            cluster_labels, cluster_centroid = \
                dataset1.cluster_scalar(X_train, num_clusters)
            dataset1._save_clusters(X_train, X_train_label,
                                    cluster_labels, cluster_centroid)
            dataset1._plot_clusters(X_train, X_train_label,
                                    cluster_labels, cluster_centroid)
        ###
        X_train = dataset1.line_reg_slopes
        X_train_label = 'Lin_regression_slopes_per_segments'
        print('-----------------------------------')
        print('Clustering ', X_train_label)
        for num_clusters in [2, 3, 4, 5, 6, 7, 8, 9, ]:
            print(num_clusters, 'clusters')
            cluster_labels, cluster_centroid = \
                dataset1.cluster_raw(X_train, num_clusters)
            dataset1._save_clusters(X_train, X_train_label,
                                    cluster_labels, cluster_centroid)
            dataset1._plot_clusters(X_train, X_train_label,
                                    cluster_labels,
                                    cluster_centroid, if_slopes=True)
        ###
        X_train = dataset1.mean_segments
        X_train_label = 'Mean_Segments'
        print('-----------------------------------')
        print('Clustering ', X_train_label)
        for num_clusters in [2, 3, 4, 5, 6, 7, 8, 9, ]:
            print(num_clusters, 'clusters')
            cluster_labels, cluster_centroid = dataset1.cluster_raw(X_train,
                                                                    num_clusters)
            dataset1._save_clusters(X_train, X_train_label,
                                    cluster_labels, cluster_centroid)
            dataset1._plot_clusters(X_train, X_train_label,
                                    cluster_labels, cluster_centroid)

    if flags.if_cluster_hiararchical:
        signal = 'raw'
        dataset1.cluster_hierarchial(signal, if_save_plot=True)

    if flags.if_detrend:
        dataset1.detrend()

