#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 19:05:33 2019

@author: vbokharaie
"""

from mitfat import flags

#%% plotting a list of 2d arrays, each row being fmri time-series for each voxel
# function plots all time series in each voxel in a plot
def plot_basics(self, data_type = 'normalised'):
    """
    Plots the time-series corresponsing to each voxel, in a template that resembles the mask.
        Each layer is saved in a separeta file. 
        Layers are chosen along the dimension with least number of layers.
    """

    import matplotlib.pyplot as plt
    import matplotlib
    import os
    import numpy as np

    # which kind of signal?
    if data_type == 'normalised':
        my_data = self.data_normalised
        subfolder = '01_basics_normalised'
        print('Plot basic plots for normalised signals ...')
    elif data_type == 'raw':
        my_data = self.data
        subfolder = '01_basics_raw'
        print('Plot basic plots for raw signals ...')
    elif my_data == 'linear_reg':
        try:
            my_data = self.data_lin_reg
            subfolder = '01_basics_linear_regresseion'
            print('Plot basic plots for linear regressed signals ...')
        except:
            print('The RoiDataset object does not contain a linear regression version of the data')
            return
    # where to save?
    dir_save = os.path.join(self.dir_save, subfolder)
    print('Plot will be saved in: \n', dir_save)
    if not os.path.exists(dir_save):
        os.makedirs(dir_save)

    # general variables    
    no_voxels = self.num_voxels
#    no_time_steps = self.num_time_steps
    bbox_seq = self.bbox_mask_seq
    bbox_mean = self.bbox_data_mean
    [n_row, n_col, no_figures] = bbox_seq.shape
    y_max = np.nanmax(my_data)

    # plot params
    plt.style.use('classic')
    cmap = matplotlib.cm.get_cmap('viridis')
    fig_w = 2*n_col
    fig_h = 2*n_row
    
    #%% let's count
    idx = 0
    for cc1 in np.arange(no_figures):
        print('Saving layer', cc1+1, 'of ', no_figures, ' ...')
        fig, my_ax_all = plt.subplots(nrows=n_row, ncols=n_col, \
                                      sharey=False, figsize=(fig_w, fig_h))
        fig.tight_layout(rect=[0, 0.03, 1, 0.95], w_pad=0.02)

        for cc_r in np.arange(n_row):
            is_new_row = True
            for cc_c in np.arange(n_col):
                my_ax = my_ax_all[cc_r, cc_c]
                my_ax.grid()
                my_ax.set_ylim(0, y_max)

                if bbox_seq[cc_r, cc_c, cc1] != 0:
                    data_ind = bbox_seq[cc_r, cc_c, cc1]-1
                    my_ax.set(title='Voxel '+str(idx+1).zfill(4))
                    my_ax.plot(self.time_steps, my_data[:,data_ind], \
                               label=self.signal_name,\
                               color='black')

                    if len(self.cutoff_times)>2:
                        my_ax.set_xticks(self.cutoff_times)
                        my_ax.tick_params(axis='both', which='major', labelsize=7)
                        from matplotlib.ticker import FormatStrFormatter
                        my_ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
                        my_ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
                        for cc_t in np.arange(len(self.cutoff_times)):
                            my_ax.axvline(x=self.cutoff_times[cc_t],\
                                          color='k', linestyle='--', linewidth=1) # vertical lines
                    color_face = cmap(bbox_mean[cc_r, cc_c, cc1])
                    my_ax.set_facecolor(color_face)
                    # y label ticks only shown in the first subplot in each row
                    if not is_new_row:
                        my_ax.set_yticklabels([])
                    else:
                        is_new_row = False
                        
                    idx = idx+1
                else:
                    my_ax.axis('off') #subplots outside the mask are suppressed

        voxel_start = str(cc1*(n_row*n_col)).zfill(4)
        voxel_end = str(np.min([(cc1+1)*n_row*n_col, no_voxels])).zfill(4)
        filename = 'Voxels_'+voxel_start+'_to_'+voxel_end
        filename = os.path.join(dir_save, filename)
        fig.savefig(filename, dpi=200, figsize=(fig_w, fig_h), format='png')
        if flags.if_save_eps:
            fig.savefig(filename+'_eps', figsize=(fig_w, fig_h), format='eps')
        fig.clf()
    
    plt.close('all')
